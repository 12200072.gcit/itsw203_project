# ITSW203_Project



## Laptop price prediction 

## Problem statement

The issue is that if a user wishes to purchase a laptop, our program should be able to generate a rough pricing estimate for that laptop based on the user's preferences as laptop is one of the most selling and purchasing devices.Although it appears that the project is straightforward or that we are simply constructing a model, the dataset we have is noisy and requires extensive feature engineering and preprocessing.




## Aim

-To predict the price of a laptop on the basis of its Specifications using machine learning.
## Scope and Limitations 

- Laptop price prediction systems will cover all kinds of laptops based on the specification of the laptops.

## Link
http://zhaypo.pythonanywhere.com/
