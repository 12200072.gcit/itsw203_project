

import React, { Fragment, useEffect, useState } from "react";
import './admin.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './table.css'


import EditFurniture from "./Editfurniture";

const Furniture = ({furniture}) => {


const [selectedCategory, setSelectedCategory] = useState(null);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/furnitures')
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(error => console.error(error));
  }, []);

  const handleCategorySelect = (event) => {
    setSelectedCategory(event.target.value);
  };

  const filteredProducts = selectedCategory ? products.filter(product => product.category === selectedCategory) : products;


  const [furnitures, setFurniture] = useState([]);

  const showToastMessage = () => {
    toast.success('Successfully Deleted!', {
      position: toast.POSITION.TOP_CENTER
    });
  };


  //delete function

  const deleteFurniture = async id => {
    if(window.confirm('Do you want to remove?')){
      try {
        const deleteFurniture = await fetch(`http://localhost:5000/furnitures/${id}`, {
          method: "DELETE"
        }).then((resp)=>{
          window.alert('Successfully Deleted!')
          window.location.reload();
        })
  
        setFurniture(furnitures.filter(furniture => furniture.furn_id !== id));
      } catch (err) {
        console.error(err.message);
      }

    }
   
  };

  const getFurniture = async () => {
    try {
      const response = await fetch("http://localhost:5000/furnitures");
      const jsonData = await response.json();

      setFurniture(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getFurniture();
  }, []);

  console.log(furnitures);

  return (
    <Fragment>
       

    {/* <div className="home_content">
            <div class="container">
                <div class="table-responsive">
                <table class="table table-bordered   table-striped" style={{width:'100%',tableLayout:'auto'}}>
                <thead class="table__head">
                    <tr>
                    <th colSpan='11'>
                     <div>
                          <select value={selectedCategory} onChange={handleCategorySelect}>
                            <option value="">All products</option>
                            <option value="furniture">Furniture</option>
                            <option value="clothes">Clothes</option>
                            <option value="vehicle">Vehicle</option>
                            <option value="electronic">Electronic</option>
                          </select>
                        </div>
 
                   
                    </th>  
                    </tr>
                    <tr class="winner__table">
                
                    <th>S/N</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Location</th>
                    <th>Mobilenumber</th>
                    <th>Used</th>
                    <th>Reason for sale</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredProducts.map(furniture => (
                    <tr key={furniture.furn_id} class="winner__table">
                    <td>{furniture.furn_id}</td>    
                    <td>{furniture.title}</td>
                    <td>{furniture.price}</td>
                    <td>{furniture.location}</td>
                    <td>{furniture.number}</td>
                    <td>{furniture.used}</td>
                    <td>{furniture.reason}</td>
                    <td>
                      <img width={100} height={100} src={`http://localhost:5000/image/${furniture.image}`}/>
                    </td>
                    <td>{furniture.category}</td>
                    <td>
                        <EditFurniture furniture={furniture} />
                    </td>
                    <td>
                        <button
                        className="btn btn-danger"
                        onClick={() => deleteFurniture(furniture.furn_id)}
                        >
                        Delete
                        </button>
                    </td>
                    </tr>
                ))}

                
                  
                
                  
                 
                </tbody>
                </table>
                </div>
            </div>
    </div> */}

                    <table>
                   
                      <thead>
                      <tr>
                    <th colSpan='11'  scope="col">
                  
                          <select value={selectedCategory} onChange={handleCategorySelect}>
                            <option value="">All products</option>
                            <option value="furniture">Furniture</option>
                            <option value="clothes">Clothes</option>
                            <option value="vehicle">Vehicle</option>
                            <option value="electronic">Electronic</option>
                          </select>
                      
 
                   
                    </th>  
                    </tr>
                        <tr>
                          {/* <th scope="col">Account</th>
                          <th scope="col">Due Date</th>
                          <th scope="col">Amount</th>
                          <th scope="col">Period</th> */}
                          <th  scope="col">S/N</th>
                          <th  scope="col">Title</th>
                          <th  scope="col">Price</th>
                          <th  scope="col">Location</th>
                          <th  scope="col">Mobilenumber</th>
                          <th  scope="col">Used</th>
                          <th  scope="col">Reason for sale</th>
                          <th  scope="col">Image</th>
                          <th  scope="col">Category</th>
                          <th  scope="col">Edit</th>
                          <th  scope="col">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        {/* <tr>
                          <td data-label="Account">Visa - 3412</td>
                          <td data-label="Due Date">04/01/2016</td>
                          <td data-label="Amount">$1,190</td>
                          <td data-label="Period">03/01/2016 - 03/31/2016</td>
                        </tr>
                        <tr>
                          <td scope="row" data-label="Account">Visa - 6076</td>
                          <td data-label="Due Date">03/01/2016</td>
                          <td data-label="Amount">$2,443</td>
                          <td data-label="Period">02/01/2016 - 02/29/2016</td>
                        </tr>
                        <tr>
                          <td scope="row" data-label="Account">Corporate AMEX</td>
                          <td data-label="Due Date">03/01/2016</td>
                          <td data-label="Amount">$1,181</td>
                          <td data-label="Period">02/01/2016 - 02/29/2016</td>
                        </tr>
                        <tr>
                          <td scope="row" data-label="Acount">Visa - 3412</td>
                          <td data-label="Due Date">02/01/2016</td>
                          <td data-label="Amount">$842</td>
                          <td data-label="Period">01/01/2016 - 01/31/2016</td>
                        </tr> */}

                              {filteredProducts.map(furniture => (
                              <tr key={furniture.furn_id} class="winner__table">
                              <td data-label='S/N'>{furniture.furn_id}</td>    
                              <td data-label='Title'>{furniture.title}</td>
                              <td data-label='Price'>{furniture.price}</td>
                              <td data-label='Location'>{furniture.location}</td>
                              <td data-label='Number'>{furniture.number}</td>
                              <td data-label='Used'>{furniture.used}</td>
                              <td data-label='Reason'>{furniture.reason}</td>
                              <td data-label='Image'>
                                <img width={100} height={100} src={`http://localhost:5000/image/${furniture.image}`}/>
                              </td>
                              <td data-label='Category'>{furniture.category}</td>
                              <td>
                                  <EditFurniture furniture={furniture} />
                              </td>
                              <td>
                                  <button
                                  className="btn btn-danger"
                                  onClick={() => deleteFurniture(furniture.furn_id)}
                                  >
                                  Delete
                                  </button>
                              </td>
                              </tr>
                          ))}

                      </tbody>
                    </table>



   
   
     
    </Fragment>
  );
};

export default Furniture;