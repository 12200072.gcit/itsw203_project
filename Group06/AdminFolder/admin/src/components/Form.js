
import React, { Fragment, useState } from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const categoryOption=[
    {value:'vehicle',label:'Vehicle'},
    {value:'furniture',label:'Furniture'},
    {value:'cloth',label:'cloth'},
    {value:'electronic',label:'Electronic'},
]

const Form = () => {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [location, setLocation] = useState("");
  const [number, setNumber] = useState("");
  const [used, setUsed] = useState("");
  const [category, setCategory] = useState("");
  const [image, setImage] = useState('');
  const [reason, setReason] = useState("");
  const [loading, setLoading] = useState(false);

  const showToastMessage = () => {
    toast.success('Successfully Saved!', {
      position: toast.POSITION.TOP_CENTER
    });
  };

 
  const onSubmitForm = async (e) => {
    e.preventDefault();
   

    try {
      setLoading(true);

      const formData = new FormData();
      formData.append('title', title);
      formData.append('price', price);
      formData.append('number', number);  
      formData.append('location',location);
      formData.append('used', used);
      formData.append('category',category);
      formData.append('image', image);
      formData.append('reason', reason);
      // const body = { title, price, location, number, used, category,image,reason };
      const response = await fetch('http://localhost:5000/furnitures', {
        method: 'POST',
        // headers: { 'Content-Type': 'application/json' },
        body: formData
      });
      setLoading(false);
      window.alert('Successfully Saved!')
      window.location('/');
    } catch (err) {
      console.error(err.message);
      setLoading(false);
    }
  };

  return (
    <div>
      <form className="mt-1" onSubmit={onSubmitForm} style={{width:'100%'}}>
        <table className="table table-bordered"
        style={{width:'100%',tableLayout:'auto'}}>
           <tr>
          <th colspan="2" style={{textAlign:'center'}}>Add Product</th>
          
        </tr>
          <tbody>
            <tr>
              <td>Title</td>
              <td>
                <input
                  type="text"
                  className="form-control"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
              
              </td>
            </tr>
            <tr>
              <td>Price</td>
              <td>
                <input
                  type="text"
                  className="form-control"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </td>
            </tr>
            <tr>
              <td>PhoneNumber</td>
              <td>
                <input
                  type="text"
                  className="form-control"
                  value={number}
                  onChange={(e) => setNumber(e.target.value)}
                />
                
              </td>
            </tr>
            <tr>
              <td>Location</td>
              <td>
                <input
                  type="text"
                  className="form-control"
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                />
                
              </td>
            </tr>
            <tr>
              <td>Used</td>
              <td>
                <input
                  type="text"
                  className="form-control"
                  value={used}
                  onChange={(e) => setUsed(e.target.value)}
                />
              </td>
            </tr>
            <tr>
              <td>Category</td>
              <td>
             
            <select name="Select Category" id="category"
            style={{width:'100%',height:'35px',borderRadius:'3px',
                            border:'1px solid rgba(0, 0, 0, 0.2)',
                            fontSize:'14px',
                             color:'rgb(99, 102, 102)'}}
                value={category} onChange={e=>setCategory(e.target.value)}>
                    <option value="" >Select Category</option>
                    {categoryOption.map((option)=>(
                        <option key={option.value} value={option.value}>
                            {option.label}
                         </option>   

                    ))}

                   
                </select>


              </td>
            </tr>
          

            <tr>
              <td>Image</td>
              <td>
                <input
                style={{height:'42px'}}
                  className="form-control"
                  rows="3"
                  onChange={e=>setImage(e.target.files[0])} type="file"
                  />
              </td>
            </tr>
            <tr>
              <td>Reason For Sale</td>
              <td>
                <textarea
                  className="form-control"
                  rows="3"
                  value={reason}
                  onChange={e=>setReason(e.target.value)}
                ></textarea>
              </td>
            </tr>
          </tbody>
        </table>
        {loading ? (
        <div style={{position:'absolute',top:'290px',left:'700px',zIndex: '9999' }}>
        <div className="text-center">
          <div className="d-flex align-items-center justify-content-center my-3">
          <div className="parent-element">
            <div className="spinner-border" style={{ color:'#F7931E' }} role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
          </div>
        </div>
      </div>
        
        ) : (
          <button type="submit" className="btn btn-success">
            submit
          </button>
        )}
      </form>
    </div>
  );
};
export default Form






        

  