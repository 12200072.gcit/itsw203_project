

import React, { Fragment, useEffect, useState } from "react";
import './admin.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './table.css'


// import EditFurniture from "./Editfurniture";

const Feedbcak = ({feedback}) => {


const [selectedCategory, setSelectedCategory] = useState(null);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch('http://localhost:5000/feedbacks')
      .then(response => response.json())
      .then(data => setProducts(data))
      .catch(error => console.error(error));
  }, []);

  const handleCategorySelect = (event) => {
    setSelectedCategory(event.target.value);
  };

  const filteredProducts = selectedCategory ? products.filter(product => product.category === selectedCategory) : products;


  const [furnitures, setFurniture] = useState([]);

  const showToastMessage = () => {
    toast.success('Successfully Deleted!', {
      position: toast.POSITION.TOP_CENTER
    });
  };


  //delete function

  const deleteFeedback = async id => {
    if(window.confirm('Do you want to remove?')){
      try {
        const deleteFeedback = await fetch(`http://localhost:5000/feedbacks/${id}`, {
          method: "DELETE"
        }).then((resp)=>{
          window.alert('Successfully Deleted!')
          window.location.reload();
        })
  
        setFurniture(furnitures.filter(furniture => furniture.feed_id !== id));
      } catch (err) {
        console.error(err.message);
      }

    }
   
  };

  const getFeedback = async () => {
    try {
      const response = await fetch("http://localhost:5000/feedbacks");
      const jsonData = await response.json();

      setFurniture(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getFeedback();
  }, []);

  console.log(furnitures);

  return (
    <Fragment>
       

   

                    <table>
                   
                      <thead>
                        <tr>
                         
                          <th  scope="col">S/N</th>
                          <th  scope="col">Email</th>
                          <th  scope="col">Mobilenumber</th>
                          <th  scope="col">Feedback</th>
                          <th  scope="col">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                      

                              {filteredProducts.map(furniture => (
                              <tr key={furniture.furn_id} class="winner__table">
                              <td data-label='S/N'>{furniture.feed_id}</td>    
                              <td data-label='Title'>{furniture.email}</td>
                              <td data-label='Number'>{furniture.number}</td>
                              <td data-label='Used'>{furniture.feedback}</td>
                         
                             
                
                              <td>
                                  <button
                                  className="btn btn-danger"
                                  onClick={() => deleteFeedback(furniture.feed_id)}
                                  >
                                  Delete
                                  </button>
                              </td>
                              </tr>
                          ))}

                      </tbody>
                    </table>



   
   
     
    </Fragment>
  );
};

export default Feedbcak;