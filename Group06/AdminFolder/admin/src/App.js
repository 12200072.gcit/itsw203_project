import {Route,BrowserRouter,Routes} from 'react-router-dom'
import Admin from "./components/sidepage";
import Furniture from './components/furniturn';
import LogIn from './components/adminlogin';
import Home from './components/homepage';
import Form from './components/Form';
import FurnitureSearchResults from './components/Search';
import LoginForm from './components/adminlogin';




function App() {
  return (

     <BrowserRouter>
        <Admin/>
        <Routes>
          {/* <Route path='/' element={<LoginForm/>}/> */}
          <Route path="/admin" element={<Admin/>} />
          <Route path="/furniture" element={<Furniture/>}/>
        
        </Routes>
     </BrowserRouter>
    
    
  );
}

export default App;
