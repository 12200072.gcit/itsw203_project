import { BrowserRouter, Route, Routes } from "react-router-dom";

// import Navbar from "./components/navbar";
// import Footer from "./components/Footer";
import Furniture from "./components/Furniture";
import Home from "./components/Home";
import LoginForm from "./components/login";
import RegistrationForm from "./components/signup";


function App() {
  return (
    <BrowserRouter>
  
      <Routes>
          {/* <Navbar/> */}
      <Route exact path="/" element={<LoginForm/>}/>
      <Route path="/signup" element={<RegistrationForm/>}/>    
      <Route path="/home" element={<Home/>}></Route>
      <Route path="/furniture" element={<Furniture/>}></Route>
    </Routes>
    {/* <Footer/> */}
    </BrowserRouter>
  
  );
}

export default App;
