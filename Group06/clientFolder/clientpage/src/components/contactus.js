import React from "react";
import './contact.css'
import { FaClock, FaEnvelope, FaMailBulk, FaPhone, FaTimes, FaWhatsapp } from "react-icons/fa";


export default function Contact(){

    const handleCall = () => {
        const phoneNumber = '77761949';
        const message = 'Hello, this is a WhatsApp message.';
    
        const url = `https://wa.me/${phoneNumber}?text=${encodeURIComponent(message)}`;
        window.open(url, '_blank');
      };

    const  handleClick = () => {
        const email = '12200072.gcit@rub.edu.bt';
        const subject = 'Hello';
        const body = 'This is the content of the email.';
    
        const mailtoUrl = `mailto:${email}?subject=${encodeURIComponent(subject)}&body=${encodeURIComponent(body)}`;
    
        window.location.href = mailtoUrl;
      };
    return(
        <div class="contact-page-sec" style={{background:'white',border:'1px solid #dfdfdf',
        marginTop:'2rem'}}>
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="contact-info">
                <div class="contact-info-item">
                  <div class="contact-info-icon">
                  
                    <FaWhatsapp color="white" style={{cursor:'pointer'}} size={25} onClick={handleCall}/>

                
                  
                  </div>
                  <div class="contact-info-text">
                    <span style={{ fontFamily:"Titillium Web, sans-serif",fontSize:'20px'}}>77761949 </span> 
                    <span style={{fontFamily:"Titillium Web, sans-serif",fontSize:'20px',
}}>GCIT ,Mongar</span> 
                  </div>
                </div>            
              </div>          
            </div>          
            <div class="col-md-4">
              <div class="contact-info">
                <div class="contact-info-item">
                  <div class="contact-info-icon">
                  <FaEnvelope style={{color:'white',cursor:'pointer'}}  size={25}
                  onClick={handleClick}/>
                  </div>
                  <div class="contact-info-text">
                    <span  style={{ fontFamily:"Titillium Web, sans-serif",fontSize:'20px'}}>info@secondhand.com</span> 
                    <span  style={{ fontFamily:"Titillium Web, sans-serif",fontSize:'20px'}}>pema@gmail.com</span>
                  </div>
                </div>            
              </div>                
            </div>                
            <div class="col-md-4">
              <div class="contact-info">
                <div class="contact-info-item">
                  <div class="contact-info-icon">
                  <FaClock color="white"  size={25}/>
                  </div>
                  <div class="contact-info-text">
                    <span  style={{ fontFamily:"Titillium Web, sans-serif",fontSize:'20px'}}>Mon - Thu  9:00 am - 4.00 pm</span>
                    <span  style={{ fontFamily:"Titillium Web, sans-serif",fontSize:'20px'}}>Sat - Sun  10.00 pm - 5.00 pm</span>
                  </div>
                </div>            
              </div>          
            </div>          
          </div>
          <div class="row">
          
            <div class="col-md-4">        
              <div class="contact-page-map">
                <iframe 
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5163.716539918308!2d91.19451771062073!3d27.239436991776305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x375eaa1550000001%3A0xc997cc9c1da87334!2sGyelpozhing%20Higher%20Secondary%20School!5e0!3m2!1sen!2sbt!4v1684833870841!5m2!1sen!2sbt"  
                    loading="lazy" referrerpolicy="no-referrer-when-downgrade"

                    width="313%" height="500" frameborder="0"  allowfullscreen
                    color="red">
                </iframe>
              </div>          
            </div>        
          </div>
        </div>
      </div>
    )
}