import { useRef,useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import GridLayout from  "react-grid-layout";
import "./main.css";
import Logo from '../assets/shop-01-removebg-preview.png'
import { Modal, NavLink} from 'react-bootstrap';
import { useNavigate } from "react-router-dom";

function Navbar() {

    const [showConfirmation, setShowConfirmation] = useState(false);
    const navigate=useNavigate()

  

    const handleLogout = () => {
        const confirmLogout = window.confirm("Are you sure you want to logout?");
        if (confirmLogout) {
          navigate('/')
          // Perform your logout logic here
          // Clear any user-related data or tokens from storage
          // Redirect the user to the login page or any other desired page
        }
      };
 

	const navRef = useRef();
   
      

	const showNavbar = () => {
		navRef.current.classList.toggle(
			"responsive_nav"
		);
	};

	return (
        <>
        <div class="container" style={{borderBottom:'1px solid #F7931E'}}>
            <div class="row">
                <div class="col">
                
                </div>
                <div class="col" style={{textAlign:'center'}}>
                 <img  src={Logo} width={200} height={100} alt=""/>
                </div>
                <div class="col">
             </div>
            </div>
        </div>

        <header>
        <h3></h3>
        <nav ref={navRef}>
            <a style={{fontSize:'24px',color:'#808080'}}>Home</a>
            <a  style={{fontSize:'24px',color:'#808080'}} href="#about">About Us</a>
            <a  style={{fontSize:'24px',color:'#808080'}} href="#contact">Contact Us</a>
            <a  style={{fontSize:'24px',color:'#808080'}} href="#feedback">Feedback</a>
            <NavLink  style={{fontSize:'24px',color:'#808080'}} onClick={handleLogout}>
                Logout</NavLink>
            <button
                className="nav-btn nav-close-btn"
                onClick={showNavbar}>
                <FaTimes />
            </button>
        </nav>
        <button
            className="nav-btn"
            onClick={showNavbar}>
            <FaBars />
        </button>
        <Modal
        isOpen={showConfirmation}
        onRequestClose={() => setShowConfirmation(false)}
        className="modal"
        overlayClassName="overlay"
      >
        <h2>Logout Confirmation</h2>
        <p>Are you sure you want to logout?</p>
        <div className="modal-buttons">
          <button onClick={handleLogout}>Yes</button>
          <button onClick={() => setShowConfirmation(false)}>No</button>
        </div>
      </Modal>
      

		</header>
        </>
		
	);
}

export default Navbar;