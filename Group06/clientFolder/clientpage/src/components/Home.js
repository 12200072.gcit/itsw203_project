import React,{useEffect,useState} from 'react'
import './Home.css'
import mobileImage from '../assets/Artboard_1-removebg-preview.png'
import desktopImage from '../assets/Artboard_1-removebg-preview.png'
import { Tab,Tabs,TabList,TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'
import './productcard.css'
import './form.css'
import About from './aboutus'
import Contact from './contactus'
import { Modal} from 'react-bootstrap';
import { AiOutlineClose } from 'react-icons/ai'
import {CiLocationOn } from 'react-icons/ci'
import { FaPhone } from 'react-icons/fa'
import gif from '../assets/66787-call.gif'
import gif1 from '../assets/75118-share-icon-animation.gif'
import {WhatsappShareButton , FacebookShareButton, TwitterShareButton } from 'react-share';
import { ToastContainer,toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Navbar from './navbar'
import Footer from './Footer'
import bk from '../assets/WhatsApp Image 2023-05-31 at 10.34.59 AM.jpeg'
import { useNavigate } from 'react-router-dom'
import styles from './styles.module.css'
import styled from 'styled-components'













function Home(){

   
const [showModal, setShowModal] = useState(false);
const [selectedFurniture, setSelectedFurniture] = useState(null);

const [email, setEmail] = useState("");
const [number, setNumber] = useState("");
const [feedback, setFeedback] = useState("");
const [loading, setLoading] = useState(false);

const [isLoggedIn, setIsLoggedIn] = useState(true); // Set the initial login state
const navigate = useNavigate();

const [isLoading, setIsLoading] = useState(false);




const showToastMessage = () => {
  toast.success('Successfully Saved!', {
    position: toast.POSITION.TOP_CENTER
  });
};


const onSubmitForm = async (e) => {
  e.preventDefault();
  try {
    setLoading(true);

    const formData = new FormData();
    formData.append('email', email);
    formData.append('number', number);  
    formData.append('feedback',feedback);
    console.log(formData)
 
    // const body = { title, price, location, number, used, category,image,reason };
    const response = await fetch('http://localhost:5000/feedbacks', {
      method: 'POST',
      // headers: { 'Content-Type': 'application/json' },
      body: formData
    });
    setIsLoading(true)
    window.alert('Successfully Saved!')
    window.location('/');
  } catch (err) {
    console.error(err.message);
    setIsLoading(false)
   
  }
};




const handleFurnitureClick = (furniture) => {
  setSelectedFurniture(furniture);
  setShowModal(true);
};




  const headingStyle = {
    textAlign: 'center',
    position: 'relative',
    lineHeight: 2,
    fontFamily:'fantasy',
    fontWeight:'200',
    fontSize:'2vw',
    color:'#808080'
  };
  
  const beforeStyle = {
    content: '""',
    position: 'absolute',
    width: '100%',
    height: '1px',
    top: 'auto',
    left: 0,
    bottom: 0,
    right: 0,
    margin: '0 auto',
    backgroundColor:'#dfdfdf',
  };
  
  const afterStyle = {
    content: '""',
    position: 'absolute',
    width: '10%',
    height: '2px',
    top: 'auto',
    left: 0,
    right: 0,
    bottom: 0,
    margin: '0 auto',
    backgroundColor: '#F7931E',
    transition: 'all 0.3s ease 0s'
  };
  
  const hoverAfterStyle = {
    width: '30%'
  };

  const Button = styled.button`
  outline: 0;
  background:#F7931E;
  width: 5%;
  border:0;
  border-radius:3px;
  padding: 10px;
  color: #ffffff;
  font-size: 15px;
  transition: all 0.4s ease-in-out;
  cursor: pointer;

  &:hover,
  &:active,
  &:focus {
    background:#F7931E;
    color:#F7931E;
    border: 1px solid #F7931E;
  }
`;


  
  
    const imageUrl = window.innerWidth >= 60 ? desktopImage : mobileImage;
    const [products, setProducts] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState(null);
    const [key, setKey] = useState('Product');
    const [leftValue, setLeftValue] = useState(0);

   

    
  
  
    
    
    useEffect(() => {
      fetch('http://localhost:5000/furnitures')
        .then(response => response.json())
        .then(data => setProducts(data))
        .catch(error => console.error(error));
    }, []);
    console.log(products)
    
    const handleCategorySelect = (category) => {
      setSelectedCategory(category);
    }
    
    const filteredProducts = selectedCategory ? products.filter(product => product.category === selectedCategory) : products;
    
    
    
      const [furnitures, setFurniture] = useState([]);
    
      
      const getFurniture = async () => {
        try {
          const response = await fetch("http://localhost:5000/furnitures");
          const jsonData = await response.json();
    
          setFurniture(jsonData);
        } catch (err) {
          console.error(err.message);
        }
      };
    
      useEffect(() => {
        getFurniture();
      }, []);
    
      console.log(furnitures);



      
// Timeout duration in milliseconds
const SESSION_TIMEOUT_DURATION = 1000000; // 5 minutes


const handleSessionTimeout = () => {
  setIsLoggedIn(false);
  notify('Due to inactivity, you have been logged out.');
  navigate('/');
};

useEffect(() => {
  let timeoutId;

  // Start the session timeout timer
  const startSessionTimeout = () => {
    timeoutId = setTimeout(handleSessionTimeout, SESSION_TIMEOUT_DURATION);
  };

  // Reset the session timeout on user activity
  const resetSessionTimeout = () => {
    clearTimeout(timeoutId);
    startSessionTimeout();
  };

  // Attach event listeners to reset session timeout on user activity
  window.addEventListener('mousemove', resetSessionTimeout);
  window.addEventListener('keypress', resetSessionTimeout);

  // Start the session timeout timer when the component mounts
  startSessionTimeout();

  // Clean up event listeners and clear the timeout when the component unmounts
  return () => {
    clearTimeout(timeoutId);
    window.removeEventListener('mousemove', resetSessionTimeout);
    window.removeEventListener('keypress', resetSessionTimeout);
  };
}, []);

if (!isLoggedIn) {
  navigate('/');
  return null;
}

const notify = (message) => {
  toast.error(message, {
    position: 'top-center',
    autoClose: 5000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
};


   
    return(
        
        <div>
        <Navbar/>
       
      <section style={{background:'white'}}>
        <div class="container container-fluid homepage-bgimage">
            <h1 class="display-4 text-center"
            style={{
              fontFamily:'fantasy',
            fontWeight:'200',
            fontSize:'36px',
            letterSpacing:'2px',
            position:'relative',
            top:'5rem',
            color:'#808080',
           }}>Second Hand Buying And Selling
            </h1>
            <p class="lead text-center"
            style={{  fontFamily:'Roboto, sans-serif',fontSize:'24px',
            color:'#808080',position:'relative',top:'6rem'}}>
             Buying Second Hand Items Can Be A Great Way To Save Money And Reduce Waste.
            </p>

            <img src={mobileImage} class="img-fluid" style={{width:'200%',height:'650px',

        position: 'relative'}} alt="..."></img>
        
        </div>
        </section>

        <div class="container container-fluid homepage-bgimage" style={{marginTop:'5rem',}}>
       
            <h1 style={headingStyle}>
              Category
              <span style={beforeStyle}></span>
              <span style={afterStyle}></span>
            </h1>

           
          
        <div class="container container-fluid homepage-bgimage" style={{background:'white',
      border:'1px solid #dfdfdf',  borderBottom:'1px solid white',marginTop:'2rem',}}>
          <Tabs
           id="controlled-tab-example"
           activeKey={key}
           style={{marginTop:'1.5rem'}}
           
           onSelect={(k) => setKey(k)}
           className="mb-3"
           >
           
           <TabList
               style={{ 
                  
                   position:'relative',
                   width:'100%',
                  
               
                  
                  
                    }}
                  
                   >


            <div>    
              
            <Tab
             
             eventKey="Product"
               style={{
               fontSize:'24px',
               color:'#808080',
               fontFamily:"Titillium Web, sans-serif",
               fontWeight:'300',
           
              
              
           }}
           onClick={() => handleCategorySelect('')}>
                  All products
               </Tab>  
                   
  
 
    
               <Tab eventKey="Vehicle"
    
               onClick={() => handleCategorySelect('vehicle')}
               style={{letterSpacing:'1px',fontSize:'24px',
               color:'#808080',
            
              
               fontFamily:"Titillium Web, sans-serif"}}
               
               >
                   Vehicle
                   
               </Tab>   
 
     
     
               <Tab  eventKey="Electronic" 
            
               onClick={() => handleCategorySelect('electronic')}
               style={{letterSpacing:'1px',fontSize:'24px',
               color:'#808080',
                  

                   fontFamily:"Titillium Web, sans-serif"}}>
               
                   Electronic
               </Tab>
               <Tab eventKey="Furniture"
        
               onClick={() => handleCategorySelect('furniture')}
               style={{letterSpacing:'1px',
               fontFamily:"Titillium Web, sans-serif",fontSize:'24px',
               color:'#808080',
            }}
               >
                   Furnitures
               
               </Tab>
               <Tab eventKey="Cloth"
          
               onClick={() => handleCategorySelect('clothes')}
               style={{letterSpacing:'1px',fontSize:'24px',
               color:'#808080',
               fontFamily:"Titillium Web, sans-serif"}}  >
                   Clothes
                   
               </Tab>
               </div>
               </TabList>
       </Tabs> 
       </div>

    
<section> 
  <div class="container" style={{background:'white', borderTop:'1px solid white',
  border:'1px solid #dfdfdf',height:'1000px'}}>
  <main class="grid">
    
  {filteredProducts.map(furniture => (
    <article  key={furniture.furn_id} style={{background:'white',boxShadow:'rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px'}}>
      <img  src={`http://localhost:5000/image/${furniture.image}`} alt="Sample photo"/>
      <div class="text">
        <h5  style={{fontSize:'24px',
        fontFamily:"Titillium Web,sans-serif",fontWeight:'400', color:'#808080',}}>Nu.{furniture.price}</h5>
        <p  style={{fontSize:'20px',
        color:'#808080',
        fontFamily:"Titillium Web, color:'#808080', sans-serif",fontWeight:'400'}}>{furniture.title}</p>
        <a  class="btn  btn-block"  onClick={() =>handleFurnitureClick(furniture)}
        style={{fontSize:'24px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'200'}}>
            View Details
        </a>
      </div>
    </article>
    
      ))} 
      
  </main>
</div>
<Modal show={showModal} onHide={() => setShowModal(false)}  size='lg'
style={{marginTop:'2rem'}}>
  <Modal.Header>
    <AiOutlineClose size={32} color='red' onClick={() => setShowModal(false)}/>
  </Modal.Header>

  <Modal.Body>
    {selectedFurniture && (
      <table class="table" style={{border:'1px white'}}>
  <tbody>
 
    <tr>
      <th scope="row" rowSpan='6'align='center' 
      style={{width:'65%'}}>
      <img  style={{height:'300px'}} src={`http://localhost:5000/image/${selectedFurniture.image}`} 
        class="rounded"  alt="..."></img>
      </th>
    </tr>
    <tr>
      <td style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400', color:'#808080',}} scope="row">Nu.{selectedFurniture.price}</td>
    </tr>
    <tr>
      <td style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400', color:'#808080',}} scope="row">{selectedFurniture.title}</td>
     
    </tr>
    <tr>
      <td style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400', color:'#808080',}} scope="row"><CiLocationOn size={32} color='green'/> {selectedFurniture.location}</td>
    </tr>
    <tr>
      <td style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400', color:'#808080',}} scope="row">Used: {selectedFurniture.used}</td>
    
    </tr>
    <tr>
      <td style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400'}} scope="row"><FaPhone size={24} color='orange'/>  {selectedFurniture.number}</td>
    </tr>
    <tr>
    <th style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400', color:'#808080',}} colSpan='2'>Reason for sale:<br></br>
    <td style={{fontSize:'20px',
        fontFamily:"Titillium Web, sans-serif",fontWeight:'400'}} scope="row">{selectedFurniture.reason}</td>
    </th>
    </tr>
    
  </tbody>

</table>

    )}
  </Modal.Body>
  <Modal.Footer>
  <table class="table" style={{border:'1px white'}}>

  <tbody style={{border:'1px white'}}>
    <tr>
      <td align='left'>
          <img width={70} src={gif}/>   
      </td>
      <td align='right'>
        <img width={70} src={gif1}></img>
      </td>
     
    </tr>
  </tbody>
</table>
  </Modal.Footer>
</Modal>


      
</section>  
     

     
     

      </div>
      <section id='about' style={{marginTop:'5rem'}}>
        
      <div class="container container-fluid homepage-bgimage">
          
      <h1 style={headingStyle}>
              About Us
              <span style={beforeStyle}></span>
              <span style={afterStyle}></span>
      </h1>
         
            <About/>
           
      </div>      
    
      </section>
           
      <section style={{marginTop:'5rem'}} id='contact'>
      <div class="container container-fluid homepage-bgimage">
      <h1 style={headingStyle}>
              Contact Us
              <span style={beforeStyle}></span>
              <span style={afterStyle}></span>
            </h1>
          <Contact/>
      </div>    
       
      </section>




      <section style={{marginTop:'5rem'}} id='feedback'>

      <div class="container container-fluid homepage-bgimage">
          
      <h1 style={headingStyle}>
              Feedback
              <span style={beforeStyle}></span>
              <span style={afterStyle}></span>
            </h1>
      </div>   
      <div class="container container-fluid homepage-bgimage" style={{marginTop:'2rem',background:'white',border:'1px solid #dfdfdf'}}>
        <div class="container" style={{marginLeft:'15rem'}}>
    
      
            <div class="col-md-8">
              <div class="contact-page-form" method="post">
                <form onSubmit={onSubmitForm}>
                  <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="single-input-field">
                      <input style={{fontSize:'18px'}} type="email" placeholder="E-mail" name="email" required
                     
                       className="form-control"
                       value={email}
                       onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                  </div>                              
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="single-input-field">
                      <input style={{fontSize:'18px'}} type="numeric" placeholder="Phone Number" name="phone"
                       
                       className="form-control"
                       value={number}
                       onChange={(e) => setNumber(e.target.number)}/>
                    </div>
                  </div>                  
                  <div class="col-md-12 message-input">
                    <div class="single-input-field">
                      <textarea 
                      style={{fontSize:'18px'}} placeholder="Write Your Feedback" name="message"
                       type="text"
                       className="form-control"
                       value={feedback}
                       onChange={(e) => setFeedback(e.target.value)}></textarea>
                    </div>
                  </div>                                                
                  <div class="single-input-fieldsbtn">
                  <Button style={{marginTop:'4rem',padding:'15px',width:'25%'}} type="submit">
                    {isLoading ?'Loading...':'Send Now'}</Button>
                  </div>                          
                </div>
                </form>   
              </div>      
            </div>  
               
        </div>
      </div>
      </section>
      <Footer/>
      
     
     
  

        </div>

       
               
    
    )
}
export default Home