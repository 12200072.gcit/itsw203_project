import React from "react";
import BackIMAGE from '../assets/blob-haikei .png'
import Logo from '../assets/shop-01-removebg-preview.png'

export default function About(){
    return(
<div style={{height:'33rem',background:'white',border:'1px solid #dfdfdf',marginTop:'2rem'}}>
  <div class="row">
    <div class="col-md-6 text-center">
        
      <img src={Logo} class="img-fluid" alt=""/>
    </div>
    <div class="col-md-6 pt-5">
    
    <p style={{lineHeight:'1.5',   fontFamily:"Titillium Web, sans-serif",fontSize:'24px',
  color:'#808080'}}>
    Welcome to our second-hand shopping platform, where we believe in the power of sustainability and conscious consumerism. Our mission is to provide a convenient and affordable way for everyone to shop for high-quality second-hand items.
    <br></br>
    <br></br>

    Our platform is designed to make second-hand shopping easy and accessible for everyone. We carefully curate a wide selection of items, from clothing and accessories to home decor and furniture, all in great condition and at affordable prices.
    </p>
      
   
    </div>
  </div>
</div>





    )
}