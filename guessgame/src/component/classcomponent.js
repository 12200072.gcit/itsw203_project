
import React, { Component } from 'react'


 
class Guessnumber extends Component{ 

  constructor(props){
    super(props)
    this.state = {
        count:0,
        guess:''}
    this.handleChange = this.handleChange.bind(this)
  }
 
  handleChange(event){
    this.setState({guess:event.target.value})
  } 
  render(){
  
      const checkNumber =()=>{
        const randomNumber = Math.floor(Math.random() * 10)+1;
        if (Number(this.state.guess) === randomNumber){
            this.setState({count:this.state.count+1});
            
            }
      }
    return (
      <div>
        <div>
          <h1>classcomponent</h1>
          <label>
            What number (between 1 and 10) am I thinking of?
          </label>
        </div>
       
         <input value={this.state.guess} 
             type="number" 
             min="1" 
             max="10" 
             onChange={this.handleChange} 
        />
        <button onClick={checkNumber}>Guess!</button>
        <p>Your score:{this.state.count}</p>
      </div>
    )
  }
}
export default Guessnumber